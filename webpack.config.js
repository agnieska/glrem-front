const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const config = require("sapper/config/webpack.js")
const webpack = require("webpack")

const pkg = require("./package.json")

const clientEntry = config.client.entry()
const intlModules = global.Intl ? [] : ["intl", "intl/locale-data/jsonp/fr.js"]
clientEntry.main = ["@babel/polyfill", ...intlModules, clientEntry.main]
const serverEntry = config.server.entry()
serverEntry.server = ["@babel/polyfill", serverEntry.server]

const mode = process.env.NODE_ENV || "production"
const dev = mode === "development"

module.exports = {
  client: {
    entry: clientEntry,
    output: config.client.output(),
    resolve: {
      extensions: [".js", ".json", ".html"],
      mainFields: ["svelte", "module", "browser", "main"],
    },
    module: {
      rules: [
        {
          test: /\.html$/,
          use: [
            {
              loader: "babel-loader",
              options: {
                cacheDirectory: true,
              },
            },
            {
              loader: "svelte-loader",
              options: {
                dev,
                hydratable: true,
                hotReload: true,
              },
            },
            {
              loader: "eslint-loader",
              options: {
                emitWarning: dev,
                fix: true,
              },
            },
          ],
        },
        {
          test: /\.(css)$/,
          use: [
            "style-loader",
            {
              // Translate CSS into CommonJS modules.
              loader: "css-loader",
              options: {
                importLoaders: 1, // Number of loaders applied before CSS loader
              },
            },
            // {
            //   loader: "postcss-loader", // Run post css actions
            //   options: {
            //     plugins: function() {
            //       // post css plugins, can be exported to postcss.config.js
            //       return [
            //         require("postcss-import"),
            //         require("tailwindcss"),
            //         require("postcss-preset-env")({ stage: 1 }),
            //       ]
            //     },
            //   },
            // },
          ],
        },
        {
          test: /\.js$/,
          use: [
            {
              loader: "babel-loader",
              options: {
                cacheDirectory: true,
                compact: true,
              },
            },
            {
              loader: "eslint-loader",
              options: {
                emitWarning: dev,
                fix: true,
              },
            },
          ],
        },
      ],
    },
    mode,
    plugins: [
      dev && new webpack.HotModuleReplacementPlugin(),
      new webpack.DefinePlugin({
        "process.browser": true,
        "process.env.NODE_ENV": JSON.stringify(mode),
      }),
    ].filter(Boolean),
    devtool: dev && "inline-source-map",
  },
  server: {
    entry: serverEntry,
    output: config.server.output(),
    target: "node",
    resolve: {
      extensions: [".js", ".json", ".html"],
      mainFields: ["svelte", "module", "browser", "main"],
    },
    externals: Object.keys(pkg.dependencies).concat("encoding"),
    module: {
      rules: [
        {
          test: /\.html$/,
          use: {
            loader: "svelte-loader",
            options: {
              css: false,
              dev,
              generate: "ssr",
            },
          },
        },
        {
          test: /\.(css)$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              // Translate CSS into CommonJS modules.
              loader: "css-loader",
              options: {
                importLoaders: 1, // Number of loaders applied before CSS loader
              },
            },
            {
              loader: "postcss-loader", // Run post css actions
              options: {
                plugins: function() {
                  // post css plugins, can be exported to postcss.config.js
                  return [
                    require("postcss-import"),
                    require("tailwindcss"),
                    require("postcss-preset-env")({ stage: 1 }),
                  ]
                },
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: "../../../static/[name].css",
        chunkFilename: "[id].css",
      }),
    ],
    mode,
    performance: {
      hints: false, // it doesn't matter if server.js is large
    },
  },
}
