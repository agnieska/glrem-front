const COMPER = {
  "PO419604": "Affaires culturelles et éducation",
  "PO419610": "Affaires économiques",
  "PO59047":  "Affaires étrangères",
  "PO420120": "Affaires sociales",
  "PO59046":  "Défense",
  "PO419865": "Développement durable",
  "PO59048":  "Finances",
  "PO59051":  "Lois",
}

const CNPS = {
  "PO744107": "Commission spéciale sur la société de confiance",
  "PO757134": "Commission spéciale sur la croissance et la transformation des entreprises",
  "PO760148": "Commission spéciale retrait du Royaume-Uni de l'Union européenne",
}

const commAbrev = {
  "CEDU":    "PO419604",
  "ECO":     "PO419610",
  "AFETR":   "PO59047",
  "SOC":     "PO420120",
  "DEF":     "PO59046",
  "DVP":     "PO419865",
  "FIN":     "PO59048",
  "LOIS":    "PO59051",
  "SEANCE":  "SEANCE",

  "CSCONF":  "PO744107",
  "CSPACTE": "PO757134",
  "CSBREXIT": "PO760148",
}

const commAbrevInv = {
  "PO419604": "CEDU",
  "PO419610": "ECO",
  "PO59047":  "AFETR",
  "PO420120": "SOC",
  "PO59046":  "DEF",
  "PO419865": "DVP",
  "PO59048":  "FIN",
  "PO59051":  "LOIS",
  "SEANCE":   "SEANCE",

  "PO744107": "CSCONF",
  "PO757134": "CSPACTE",
  "PO760148": "CSBREXIT",
}

export { COMPER, CNPS, commAbrev, commAbrevInv }

// vim: set ts=2 sw=2 sts=2 et:
