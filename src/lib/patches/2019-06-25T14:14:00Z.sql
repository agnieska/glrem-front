CREATE TABLE IF NOT EXISTS sudoers (
  sudo_name_id text NOT NULL PRIMARY KEY,
  sudo_id_tribun text,
  sudo_role user_role
);
COMMENT ON TABLE sudoers IS 'utilisateurs pouvant prendre l''identité d''un autre utilisateur';
COMMENT ON COLUMN sudoers.sudo_name_id IS 'identifiant unique utilisé pour s''identifier sur le site de l''Assemblée';
COMMENT ON COLUMN sudoers.sudo_id_tribun IS 'code d''un député à l''Assemblée à incarner';
COMMENT ON COLUMN sudoers.sudo_role IS 'rôle (fonction) de l''utilisateur à incarner';
