const ordinaux = {
  1: "premier",
  2: "deuxième",
  3: "troisième",
  4: "quatrième",
  5: "cinquième",
  6: "sixième",
  7: "septième",
  8: "huitième",
  9: "neuvième",
  10: "dixième",
  11: "onzième",
  12: "douzième",
  13: "treizième",
  14: "quatorzième",
  15: "quinzième",
  16: "seizième",
  17: "dix-septième",
  18: "dix-huitième",
  19: "dix-neuvième",
  20: "vingtième",
  30: "trentième",
  40: "quarantième",
  50: "cinquantième",
  60: "soixantième",
  70: "soixante-dixième",
  80: "quatre-vingtième",
  90: "quatre-vingt-dixième",
}

function preFilledAmendment({
  division,
  division_place,
  alinea,
  alinea_place,
}) {
  const regexArticle = /^Article ./,
    regexAlinea = /^[1-9][0-9]*$/
  if (division_place === "SUPPRESSION") {
    return "Supprimer cet article."
  } else if (
    division_place === "REDACTION_GLOBALE" &&
    regexArticle.test(division)
  ) {
    return "L'article " + division.substring(8) + " est ainsi rédigé :\n« "
  } else if (division_place === "AVANT" && regexArticle.test(division)) {
    return (
      "Avant l'article " +
      division.substring(8) +
      ", insérer un article ainsi rédigé :\n« "
    )
  } else if (division_place === "APRES" && regexArticle.test(division)) {
    return (
      "Après l'article " +
      division.substring(8) +
      ", insérer un article ainsi rédigé :\n« "
    )
  } else if (
    division_place === "A" &&
    regexAlinea.test(alinea) &&
    Number(alinea) < 100 &&
    regexArticle.test(division)
  ) {
    if (alinea_place === "A") {
      return (
        "Au " +
        countOrdinal(alinea) +
        " alinéa de l'article " +
        division.substring(8)
      )
    } else if (alinea_place === "AVANT") {
      return (
        "Avant le " +
        countOrdinal(alinea) +
        " alinéa de l'article " +
        division.substring(8)
      )
    } else if (alinea_place === "APRES") {
      return (
        "Après le " +
        countOrdinal(alinea) +
        " alinéa de l'article " +
        division.substring(8)
      )
    }
  }
  return null
}

function countOrdinal(n) {
  if (n in ordinaux) {
    return ordinaux[n]
  } else if (n >= 100 || n <= 0) {
    return null
  } else {
    const n10 = Math.floor(n / 10),
      n1 = n - 10 * n10
    if (n10 === 7 || n10 === 9) {
      return (
        ordinaux[10 * n10].substring(0, ordinaux[10 * n10].length - 7) +
        (n1 === 1 ? "et-" : "") +
        ordinaux[n1 + 10]
      )
    } else if (n1 === 1) {
      return (
        ordinaux[10 * n10].substring(0, ordinaux[10 * n10].length - 4) +
        (n10 > 2 && n10 !== 8 ? "e" : "") +
        "-et-unième"
      )
    }
    return (
      ordinaux[10 * n10].substring(0, ordinaux[10 * n10].length - 4) +
      (n10 > 2 && n10 !== 8 ? "e" : "") +
      "-" +
      ordinaux[n1]
    )
  }
}

export { preFilledAmendment }

// vim: set ts=2 sw=2 sts=2 et:
