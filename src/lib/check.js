// const fields_amendment = [
//   "division",
//   "division_place",
//   "alinea",
//   "alinea_place",
//   "dispositif",
//   "expose",
// ]
const mandatory_fields_amendment = [
  "division",
  "division_place",
  "dispositif",
  "expose",
]
const division_place_values = new Set([
  "AVANT",
  "A",
  "APRES",
  "SUPPRESSION",
  "REDACTION_GLOBALE",
])
const alinea_place_values = new Set(["AVANT", "A", "APRES"])

/**
 * Check the integrity of the amendment for a given action and a given profile.
 *
 * @param str action Action type in ['draft', 'save'].
 * @param str profile Profile in ['author', 'group'].
 * @param dict Amendment data.
 * @return dict
 */
export function check_integrity_amendment(action, profile, amendment) {
  let result = { status: "success", wrong: [], missing: [] }

  // First: system check - should always pass except if hard error

  // Check there are no extra fields
  /*for( let field of amendment ) {
    if( ! field in fields_amendment ) {
      result.status = "fail"
      result.wrong.push(field)
    }
  }*/

  // A division_place MUST be in the enumerated values
  if (!(amendment["division_place"] in division_place_values)) {
    result.status = "fail"
    result.wrong.push("division_place")
  }

  // Second: user check
  if (action === "DEPOSE") {
    // If an alinea is written, the alinea_place MUST be in the enumerated values
    if (
      amendment["alinea"] &&
      !(amendment["alinea_place"] in alinea_place_values)
    ) {
      result.status = "fail"
      result.wrong.push("alinea_place")
    }

    for (let field of mandatory_fields_amendment) {
      if (!amendment[field]) {
        result.status = "fail"
        result["missing"].push(field)
      }
    }
  }

  if (action === "BROUILLON") {
    if (!amendment.division) {
        result.status = "fail"
        result["missing"].push("division")
    }
  }

  return result
}

export function sanitiseMinimalHtml(text) {
  return (text || "")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/&lt;(\/?(?:p|table|tbody|tr|td|th|figure)(?: +class="[^"]*")?)&gt;/g, "<$1>")
}

// vim: set ts=2 sw=2 sts=2 et:
