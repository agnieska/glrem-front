import { validateConfig } from "./validators/config"

const config = {
  // globalAlert: {
  //   class: "is-danger",
  //   messageHtml: `
  //     <article class="container mx-auto my-4" role="alert">
  //       <div class="warning-title">
  //         Attention&nbsp;!
  //       </div>
  //       <div class="warning-content">
  //         <p>
  //           Ce site est <b>en développement</b>. Les informations qui y figurent peuvent être fausses et surtout
  //           <b>incomplètes</b>. En cas de doute, référez-vous aux sources des différentes données.
  //         </p>
  //         <p class="leading-normal">
  //           N'hésitez pas à nous
  //           <a class="text-blue underline" href="https://framagit.org/glrem-platform/glrem-front/issues" target="_blank" title="Support technique" class="text-jaune">signaler un problème</a>.
  //         </p>
  //       </div>
  //     </article>
  //   `,
  // },
  leftMenu: [
    // {
    //   contentHtml: "Accueil",
    //   prefetch: true,
    //   title: "Tableau de bord",
    //   url: ".",
    // },
    // {
    //   contentHtml: "Annuaire",
    //   prefetch: true,
    //   title: "Trombinoscope des députés LaREM",
    //   url: "deputes",
    // },
    {
      contentHtml: "Amendements",
      prefetch: true,
      title: "Projets et propositions de lois",
      url: "textes",
    },
    // {
    //   contentHtml: "Thématiques",
    //   prefetch: true,
    //   title: "Sujet des commissions",
    //   url: "#thematiques",
    // },
    // {
    //   contentHtml: "Calendrier",
    //   prefetch: true,
    //   title: "Agenda parlementaire",
    //   url: "#calendrier",
    // },
    // {
    //   contentHtml: "Fils d'actualités",
    //   prefetch: true,
    //   title: "Fils Telegram",
    //   url: "#telegram",
    // },
  ],
  missionStatement: "Intranet groupe LaREM",
  rightMenu: [
    {
      contentHtml: "À propos",
      prefetch: true,
      title: "Informations sur ce site web",
      url: "a-propos",
    },
  ],
  supportUsersNameIds: null,
  telegram: {
    infosAttachesParlementaires: {
      id: -1001489995222,
      title: "Collabs députés",
    },
    // infosCollaborateursGroupe: {
    //   id: -1001346979214,
    //   title: "Collabs groupe",
    // },
    // infosDeputes: {
    //   id: -1001156066637,
    //   title: "Députés",
    // },
    // outils: {
    //   id: -1001213000632,
    //   title: "Outils",
    // },
  },
  title: "Intranet groupe LaREM",
  url: ".",
  webSocketUrl: "ws://localhost:3000/",
}

const [validConfig, error] = validateConfig(config)
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(
      validConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

export default validConfig
