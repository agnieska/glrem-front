/// Generate a CSV file of active LaREM députés for bulk import into ZenDesk.
/// cf https://support.zendesk.com/hc/en-us/articles/203661996-Bulk-importing-users-
/// After generating file, go to https://xxx.zendesk.com/agent/admin/people
/// and click on "Bulk user import".

import { loadAssembleeData } from "@tricoteuses/assemblee"
import { EnabledDatasets } from "@tricoteuses/assemblee/lib/config"
import {
  CodeType,
  MandatXsiType,
} from "@tricoteuses/assemblee/lib/types/acteurs_et_organes"
import assert from "assert"
import commandLineArgs from "command-line-args"
import csvStringify from "csv-stringify"

const optionsDefinitions = [
  {
    alias: "l",
    defaultValue: "15",
    name: "legislature",
    type: String,
  },
  {
    alias: "g",
    defaultValue: "PO730964",
    help: 'uid of "groupe parlementaire LaREM"',
    name: "groupeParlementaire",
    type: String,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)
const { acteurByUid, infosDeputeByUid } = loadAssembleeData(
  options.dataDir,
  EnabledDatasets.ActeursEtOrganes | EnabledDatasets.InfosDeputes,
  options.legislature,
)

async function main() {
  const deputesActifs = await retrieveDeputesActifs()
  const lines = [
    ["name", "email", "external_id", "tags"],
  ]
  for (let depute of deputesActifs) {
    const infosDepute = infosDeputeByUid[depute.uid]
    if (infosDepute === undefined) {
      console.log(`Missing informations for député ${JSON.stringify(depute)}`)
      continue
    }
    const email = infosDepute.emails.filter(email =>
      email.endsWith("@assemblee-nationale.fr"),
    )[0]
    if (email === undefined) {
      console.log(`Missing email in députés informations: ${JSON.stringify(infosDepute)}`)
      continue
    }
    lines.push([
      `${depute.etatCivil.ident.prenom} ${depute.etatCivil.ident.nom}`,
      email,
      depute.uid,
      ["député"].join(","),
    ])
  }
  const csv = await new Promise((resolve, reject) => {
    csvStringify(
      lines,
      // {
      //   delimiter: ";",
      // },
      function (err, csv) {
        if (err) {
          reject(err)
        } else {
          resolve(csv)
        }
      },
    )
  })
  console.log(csv)
}

async function retrieveDeputesActifs() {
  const dataDir = options.dataDir
  assert(dataDir, "Missing argument: data directory")
  const now = new Date()
  const deputesActifs = Object.values(acteurByUid)
    .filter(acteur => {
      const { mandats } = acteur
      if (!mandats) {
        return false
      }
      const mandatDeputeActif = mandats.filter(
        mandat =>
          mandat.xsiType === MandatXsiType.MandatParlementaireType &&
          mandat.legislature === options.legislature &&
          mandat.typeOrgane === CodeType.Assemblee &&
          mandat.dateDebut <= now &&
          (!mandat.dateFin || mandat.dateFin >= now),
      )[0]
      if (mandatDeputeActif === undefined) {
        return false
      }
      const mandatGroupeParlementaireActif = mandats.filter(
        mandat =>
          mandat.xsiType === MandatXsiType.MandatSimpleType &&
          mandat.legislature === options.legislature &&
          mandat.typeOrgane === CodeType.Gp &&
          mandat.dateDebut <= now &&
          (!mandat.dateFin || mandat.dateFin >= now) &&
          mandat.organesRefs.some(
            organeRef => organeRef == options.groupeParlementaire,
          ),
      )[0]
      if (mandatGroupeParlementaireActif === undefined) {
        return false
      }
      return true
    })
    .sort((a, b) => a.uid.localeCompare(b.uid))

  return deputesActifs
}

main().catch(error => {
  console.log(error)
  process.exit(1)
})
