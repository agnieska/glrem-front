import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import fetch from "node-fetch"
import path from "path"
import { loadAssembleeData } from "@tricoteuses/assemblee"
import { EnabledDatasets } from "@tricoteuses/assemblee/lib/config"
import { parseTexteLoiAssemblee } from "@tricoteuses/assemblee/lib/model/documents"
import {
  DocumentUrlFormat,
  urlFromDocument,
} from "@tricoteuses/assemblee/lib/model/urls"
import { DocumentXsiType } from "@tricoteuses/assemblee/lib/types/dossiers_legislatifs"
import { db } from "../database"

const optionsDefinitions = [
  {
    alias: "f",
    help: "fetch documents instead of retrieving them from files",
    name: "fetch",
    type: Boolean,
  },
  {
    alias: "l",
    defaultValue: "15",
    name: "legislature",
    type: String,
  },
  {
    alias: "p",
    help: "parse documents",
    name: "parse",
    type: Boolean,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    help: "UID of first Assemblée's document to retrieve",
    name: "uid",
    type: String,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function retrieveDocuments() {
  const dataDir = options.dataDir
  assert(dataDir, "Missing argument: data directory")
  const { documentByUid, dossierParlementaireByUid } = loadAssembleeData(
    options.dataDir,
    EnabledDatasets.DossiersLegislatifs,
    options.legislature,
  )
  let clotureSeanceBTC = {}
  const textesLois = Object.values(documentByUid)
    .filter(document => document.xsiType === DocumentXsiType.TexteLoiType)
    .filter(
      document =>
        document.uid.startsWith("PIONAN") || document.uid.startsWith("PRJLAN"),
    )
    .map(document => {
      const uidTexteLoi = document.uid,
        uidDossierParlementaire = document.dossierRef,
        dossierParlementaire =
          dossierParlementaireByUid[uidDossierParlementaire]
      let commissionFond = null,
        commissionsAvis = [],
        seance = false,
        etapeTexte = null,
        urlDossier = null,
        dateDepotTexte = {
          assemblee: null,
          commissionFond: null,
          commissionsAvis: {},
          seance: null,
        },
        clotureActeLegislatif = {
          commissionFond: null,
          commissionsAvis: {},
          seance: null,
        }
      if (/BTC[0-9]+$/.test(uidTexteLoi)) {
        // nop: these texts are managed in a latter processing
      } else {
        const dpEtapes = dossierParlementaire.actesLegislatifs,
          dpEtapeActive = dpEtapes.find(
            e =>
              e.codeActe.substring(0, 2) === "AN" &&
              ((!Array.isArray(e.actesLegislatifs) &&
                e.actesLegislatifs.texteAssocie === uidTexteLoi) ||
                (Array.isArray(e.actesLegislatifs) &&
                  e.actesLegislatifs.find(
                    t => t.texteAssocie === uidTexteLoi,
                  ) !== undefined)),
          )
        if (
          dossierParlementaire.titreDossier &&
          dossierParlementaire.titreDossier.titreChemin
        ) {
          urlDossier = dossierParlementaire.titreDossier.titreChemin
        }
        if (dpEtapeActive) {
          const aEtapes = dpEtapeActive.actesLegislatifs,
            aEtapeDepot = aEtapes.find(
              e => e.codeActe.substring(e.codeActe.length - 6) === "-DEPOT",
            )
          if (
            dpEtapeActive.libelleActe &&
            dpEtapeActive.libelleActe.nomCanonique
          ) {
            etapeTexte = dpEtapeActive.libelleActe.nomCanonique
          }
          if (aEtapeDepot && aEtapeDepot.dateActe) {
            dateDepotTexte.assemblee = aEtapeDepot.dateActe
          }
          const aEtapeCommissions = aEtapes.find(
            e => e.codeActe.substring(e.codeActe.length - 4) === "-COM",
          )
          if (aEtapeCommissions) {
            const cEtapes = aEtapeCommissions.actesLegislatifs,
              cEtapeComFond = cEtapes.find(
                e =>
                  e.codeActe.substring(e.codeActe.length - 9) === "-COM-FOND",
              ),
              cEtapeComAvis = cEtapes.filter(
                e =>
                  e.codeActe.substring(e.codeActe.length - 9) === "-COM-AVIS",
              )
            if (cEtapeComFond) {
              commissionFond = cEtapeComFond.organeRef
              const cfEtapes = cEtapeComFond.actesLegislatifs,
                cfSaisie = cfEtapes.find(
                  e =>
                    e.codeActe.substring(e.codeActe.length - 16) ===
                    "-COM-FOND-SAISIE",
                ),
                cfRapport = cfEtapes.find(
                  e =>
                    e.codeActe.substring(e.codeActe.length - 17) ===
                    "-COM-FOND-RAPPORT",
                )
              if (cfSaisie && cfSaisie.dateActe) {
                dateDepotTexte.commissionFond = cfSaisie.dateActe
              }
              if (cfRapport && cfRapport.dateActe) {
                clotureActeLegislatif.commissionFond = cfRapport.dateActe
              }
              if (
                cfRapport &&
                (!cfRapport.texteAdopte ||
                  /BTC[0-9]+$/.test(cfRapport.texteAdopte))
              ) {
                if (!cfRapport.texteAdopte) {
                  // Textes non-adoptés par la commission au fond : le texte de séance est le texte initialement déposé
                  // NB: les PLF sont bien inscrits comme non-adoptés par la commission au fond
                  seance = true
                }
                const aEtapeSeance = aEtapes.find(
                  e =>
                    e.codeActe.substring(e.codeActe.length - 7) === "-DEBATS",
                )
                if (aEtapeSeance) {
                  const sEtapes = aEtapeSeance.actesLegislatifs
                      ? aEtapeSeance.actesLegislatifs
                      : null,
                    sEtapeDecision =
                      sEtapes !== null
                        ? sEtapes.find(
                            e =>
                              e.codeActe.substring(e.codeActe.length - 11) ===
                              "-DEBATS-DEC",
                          )
                        : null
                  if (sEtapeDecision && sEtapeDecision.dateActe) {
                    if (!cfRapport.texteAdopte) {
                      clotureActeLegislatif.seance = sEtapeDecision.dateActe
                    } else {
                      clotureSeanceBTC[cfRapport.texteAdopte] =
                        sEtapeDecision.dateActe
                    }
                  }
                }
              }
            }
            if (cEtapeComAvis.length > 0) {
              commissionsAvis = cEtapeComAvis.map(c => c.organeRef)
              cEtapeComAvis.forEach(c => {
                const caEtapes = c.actesLegislatifs,
                  caSaisie = caEtapes.find(
                    e =>
                      e.codeActe.substring(e.codeActe.length - 16) ===
                      "-COM-AVIS-SAISIE",
                  ),
                  caRapport = caEtapes.find(
                    e =>
                      e.codeActe.substring(e.codeActe.length - 17) ===
                      "-COM-AVIS-RAPPORT",
                  )
                dateDepotTexte.commissionsAvis[c.organeRef] = caSaisie
                  ? caSaisie.dateActe
                  : null
                clotureActeLegislatif.commissionsAvis[c.organeRef] = caRapport
                  ? caRapport.dateActe
                  : null
              })
            }
          }
        }
      }
      return {
        ...document,
        rawDocumentAssembleeUrl: urlFromDocument(
          document,
          DocumentUrlFormat.RawHtml,
        ),
        etapeTexte,
        urlDossier,
        seance,
        commissionFond,
        commissionsAvis,
        dateDepotTexte,
        clotureActeLegislatif,
      }
    })
    // Si le texte est un BTC, sa vie législative a dû être lue auparavant et,
    // le cas échéant, la décision de la séance a été lue. Le passage par la
    // variable intermédiaire clotureSeanceBTC n’est pas élégant, mais il
    // évite d’avoir à refaire tout le chemin dans les actes législatifs pour
    // les textes BTC.
    .map(document => {
      if (/BTC[0-9]+$/.test(document.uid)) {
        document.seance = true
        if (document.uid in clotureSeanceBTC) {
          document.clotureActeLegislatif.seance = clotureSeanceBTC[document.uid]
        }
      }
      return document
    })
    .sort((a, b) =>
      a.rawDocumentAssembleeUrl === null
        ? -1
        : b.rawDocumentAssembleeUrl === null
        ? 1
        : a.rawDocumentAssembleeUrl.localeCompare(b.rawDocumentAssembleeUrl),
    )

  const firstUid = options.uid
  let skip = !!firstUid
  for (let texteLoi of textesLois) {
    if (skip) {
      if (texteLoi.uid === firstUid) {
        skip = false
      } else {
        continue
      }
    }
    if (!options.silent) {
      console.log(texteLoi.uid, texteLoi.rawDocumentAssembleeUrl)
    }

    // Write into database
    let record = {
      numNotice: texteLoi.notice.numNotice,
      titrePrincipal:
        texteLoi.titres.titrePrincipal.substring(0, 1).toUpperCase() +
        texteLoi.titres.titrePrincipal.substring(1),
      titrePrincipalCourt:
        texteLoi.titres.titrePrincipalCourt.substring(0, 1).toUpperCase() +
        texteLoi.titres.titrePrincipalCourt.substring(1),
      dateDepot: texteLoi.cycleDeVie.chrono.dateDepot,
      etapeTexte: texteLoi.etapeTexte,
      urlDossier: texteLoi.urlDossier,
      commFond: texteLoi.commissionFond,
      clotureActeLegislatif: null,
    }
    await db.one(
      `INSERT INTO textes(txt_id, txt_ordre, txt_titre, txt_titre_court, txt_etape,
                          txt_commission, txt_date_depot, txt_officiel, txt_dossier, txt_note, txt_url_note)
       VALUES ($<numNotice>, NULL, $<titrePrincipal>, $<titrePrincipalCourt>, $<etapeTexte>,
               $<commFond>, $<dateDepot>, true, $<urlDossier>, NULL, NULL)
       ON CONFLICT (txt_id)
       DO
         UPDATE
         SET
           txt_titre = $<titrePrincipal>,
           txt_titre_court = $<titrePrincipalCourt>,
           txt_etape = $<etapeTexte>,
           txt_commission = $<commFond>,
           txt_date_depot = $<dateDepot>,
           txt_dossier = $<urlDossier>
       RETURNING txt_id
      `,
      record,
    )
    if (texteLoi.seance) {
      record.clotureActeLegislatif = texteLoi.clotureActeLegislatif.seance
      await db.one(
        `INSERT INTO textes_amendables(txta_id, txta_txt, txta_commission, txta_ordre, txta_statut, txta_date_depot,
                                       txta_date_depot_limite_interne, txta_date_depot_limite_assemblee,
                                       txta_nb_amdt_deposes, txta_cloture, txta_amendable)
         VALUES (DEFAULT, $<numNotice>, 'SEANCE', NULL, NULL, NULL, NULL, NULL, 0, $<clotureActeLegislatif>, true)
         ON CONFLICT (txta_txt, txta_commission)
         DO
           UPDATE
           SET
             txta_statut = NULL,
             txta_date_depot = NULL,
             txta_cloture = $<clotureActeLegislatif>
         RETURNING txta_id
        `,
        record,
      )
    }
    if (texteLoi.commissionFond) {
      record.dateDepot = texteLoi.dateDepotTexte.commissionFond
      record.clotureActeLegislatif =
        texteLoi.clotureActeLegislatif.commissionFond
      await db.one(
        `INSERT INTO textes_amendables(txta_id, txta_txt, txta_commission, txta_ordre, txta_statut, txta_date_depot,
                                       txta_date_depot_limite_interne, txta_date_depot_limite_assemblee,
                                       txta_nb_amdt_deposes, txta_cloture, txta_amendable)
         VALUES (DEFAULT, $<numNotice>, $<commFond>, NULL, 'FOND', $<dateDepot>, NULL, NULL,
                 0, $<clotureActeLegislatif>, true)
         ON CONFLICT (txta_txt, txta_commission)
         DO
           UPDATE
           SET
             txta_statut = 'FOND',
             txta_date_depot = $<dateDepot>,
             txta_cloture = $<clotureActeLegislatif>
         RETURNING txta_id
        `,
        record,
      )
    }
    if (texteLoi.commissionsAvis.length > 0) {
      for (let commission of texteLoi.commissionsAvis) {
        record.commAvis = commission
        record.dateDepot = texteLoi.dateDepotTexte.commissionsAvis[commission]
        record.clotureActeLegislatif =
          texteLoi.clotureActeLegislatif.commissionsAvis[commission]
        await db.one(
          `INSERT INTO textes_amendables(txta_id, txta_txt, txta_commission, txta_ordre, txta_statut, txta_date_depot,
                                         txta_date_depot_limite_interne, txta_date_depot_limite_assemblee,
                                         txta_nb_amdt_deposes, txta_cloture, txta_amendable)
           VALUES (DEFAULT, $<numNotice>, $<commAvis>, NULL, 'AVIS', $<dateDepot>, NULL, NULL,
                   0, $<clotureActeLegislatif>, true)
           ON CONFLICT (txta_txt, txta_commission)
           DO
             UPDATE
             SET
               txta_statut = 'AVIS',
               txta_date_depot = $<dateDepot>,
               txta_cloture = $<clotureActeLegislatif>
           RETURNING txta_id
          `,
          record,
        )
      }
    }

    // Retrieve text document
    if (texteLoi.rawDocumentAssembleeUrl === null) {
      continue
    }
    const url = new URL(texteLoi.rawDocumentAssembleeUrl)
    const filePath = path
      .join(dataDir, url.hostname, ...url.pathname.split("/"))
      .replace(/\.asp$/, ".html")
    fs.ensureDirSync(path.dirname(filePath))

    let page = undefined
    if (options.fetch) {
      const response = await fetch(texteLoi.rawDocumentAssembleeUrl)
      page = await response.text()
      if (!response.ok) {
        if (response.status !== 404 && !options.silent) {
          console.warn(
            `Error while getting page "${
              texteLoi.rawDocumentAssembleeUrl
            }" (uid: ${texteLoi.uid}):\n\nError:\n${JSON.stringify(
              { code: response.status, message: response.statusText },
              null,
              2,
            )}`,
          )
        }
        continue
      }
      fs.writeFileSync(filePath, page)
    } else {
      try {
        page = fs.readFileSync(filePath, { encoding: "utf-8" })
      } catch (e) {
        if (e.code === "ENOENT") {
          continue
        }
        throw e
      }
    }

    if (options.parse) {
      const texteLoiParsed = parseTexteLoiAssemblee(
        texteLoi.rawDocumentAssembleeUrl,
        page,
      )
      const { error } = texteLoiParsed
      if (error) {
        if (!options.silent) {
          console.warn(
            `Error while parsing page "${
              texteLoi.rawDocumentAssembleeUrl
            }" (uid: ${texteLoi.uid}):\n\nError:\n${JSON.stringify(
              error,
              null,
              2,
            )}`,
          )
        }
        continue
      }
    }
  }
  return textesLois
}

retrieveDocuments().catch(error => {
  console.log(error)
  process.exit(1)
})

// vim: set ts=2 sw=2 sts=2 et:
