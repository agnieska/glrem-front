import assert from "assert"
import commandLineArgs from "command-line-args"
import dedent from "dedent-js"
import { loadAssembleeData } from "@tricoteuses/assemblee"
import { EnabledDatasets } from "@tricoteuses/assemblee/lib/config"
import {
  CodeType,
  MandatXsiType,
} from "@tricoteuses/assemblee/lib/types/acteurs_et_organes"

import {
  addUserToGroup,
  createUser,
  removeUserFromGroup,
  retrieveGroupUsernames,
  retrieveUsernames,
} from "../lib/nextcloud"

const deputesGroupName = "Députés LaREM"
const optionsDefinitions = [
  {
    alias: "l",
    defaultValue: "15",
    name: "legislature",
    type: String,
  },
  {
    alias: "g",
    defaultValue: "PO730964",
    help: 'uid of "groupe parlementaire LaREM"',
    name: "groupeParlementaire",
    type: String,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)
const { acteurByUid, infosDeputeByUid } = loadAssembleeData(
  options.dataDir,
  EnabledDatasets.ActeursEtOrganes | EnabledDatasets.InfosDeputes,
  options.legislature,
)

async function main() {
  throw new Error(
    dedent`
      This script is obsolete, because it uses the email of the "députés"
      instead of their samlId.
    `,
  )
  const existingNextcloudUsernames = new Set(await retrieveUsernames())
  const existingDeputesGroupUsernames = new Set(
    await retrieveGroupUsernames(deputesGroupName),
  )
  const deputesActifs = await retrieveDeputesActifs()
  for (let depute of deputesActifs) {
    const infosDepute = infosDeputeByUid[depute.uid]
    if (infosDepute === undefined) {
      continue
    }
    const email = infosDepute.emails.filter(email =>
      email.endsWith("@assemblee-nationale.fr"),
    )[0]
    if (email === undefined) {
      continue
    }
    if (existingNextcloudUsernames.has(email)) {
      // Député already has a Nextcloud account
      if (existingDeputesGroupUsernames.has(email)) {
        // Député already belongs to the group of députés.
        existingDeputesGroupUsernames.delete(email)
      } else {
        // User is not yet a member of députés group. Add it.
        console.log(`Adding ${email} from group ${deputesGroupName}`)
        await addUserToGroup(deputesGroupName, { samlId: email })
      }
    } else {
      // New député
      console.log(`Adding Nextcloud user ${email}…`)
      await createUser({ samlId: email }, [deputesGroupName])
    }
  }
  for (let username of [...existingDeputesGroupUsernames]) {
    // User is no more an active Député => Remove him from députés group.
    console.log(`Removing ${username} from group ${deputesGroupName}`)
    await removeUserFromGroup(deputesGroupName, { samlId: username })
  }
}

async function retrieveDeputesActifs() {
  const dataDir = options.dataDir
  assert(dataDir, "Missing argument: data directory")
  const now = new Date()
  const deputesActifs = Object.values(acteurByUid)
    .filter(acteur => {
      const { mandats } = acteur
      if (!mandats) {
        return false
      }
      const mandatDeputeActif = mandats.filter(
        mandat =>
          mandat.xsiType === MandatXsiType.MandatParlementaireType &&
          mandat.legislature === options.legislature &&
          mandat.typeOrgane === CodeType.Assemblee &&
          mandat.dateDebut <= now &&
          (!mandat.dateFin || mandat.dateFin >= now),
      )[0]
      if (mandatDeputeActif === undefined) {
        return false
      }
      const mandatGroupeParlementaireActif = mandats.filter(
        mandat =>
          mandat.xsiType === MandatXsiType.MandatSimpleType &&
          mandat.legislature === options.legislature &&
          mandat.typeOrgane === CodeType.Gp &&
          mandat.dateDebut <= now &&
          (!mandat.dateFin || mandat.dateFin >= now) &&
          mandat.organesRefs.some(
            organeRef => organeRef == options.groupeParlementaire,
          ),
      )[0]
      if (mandatGroupeParlementaireActif === undefined) {
        return false
      }
      return true
    })
    .sort((a, b) => a.uid.localeCompare(b.uid))

  const firstUid = options.uid
  let skip = !!firstUid
  for (let depute of deputesActifs) {
    if (skip) {
      if (depute.uid === firstUid) {
        skip = false
      } else {
        continue
      }
    }
  }

  return deputesActifs
}

main().catch(error => {
  console.log(error)
  process.exit(1)
})
