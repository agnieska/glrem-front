import { setupTelegram, telegramClient } from "../lib/telegram"

async function main() {
  await setupTelegram()
  console.log(JSON.stringify(await telegramClient.tg.getAllChats(), null, 2))
  process.exit(0)
}

main().catch(error => {
  console.log(error)
  process.exit(1)
})
