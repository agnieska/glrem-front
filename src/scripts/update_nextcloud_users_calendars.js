import assert from "assert"
import commandLineArgs from "command-line-args"
import dedent from "dedent-js"
import { loadAssembleeData } from "@tricoteuses/assemblee"
import { EnabledDatasets } from "@tricoteuses/assemblee/lib/config"
import {
  CodeType,
  MandatXsiType,
} from "@tricoteuses/assemblee/lib/types/acteurs_et_organes"

import {
  retrieveUserCalendars,
  retrieveUsernames,
  shareCalendar,
} from "../lib/nextcloud"
import serverConfig from "../server_config"

const { nextcloud } = serverConfig
const optionsDefinitions = [
  {
    alias: "l",
    defaultValue: "15",
    name: "legislature",
    type: String,
  },
  {
    alias: "g",
    defaultValue: "PO730964",
    help: 'uid of "groupe parlementaire LaREM"',
    name: "groupeParlementaire",
    type: String,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)
const { acteurByUid, infosDeputeByUid, organeByUid } = loadAssembleeData(
  options.dataDir,
  EnabledDatasets.ActeursEtOrganes | EnabledDatasets.InfosDeputes,
  options.legislature,
)

async function main() {
  throw new Error(
    dedent`
      This script is obsolete, because it uses the email of the "députés"
      instead of their samlId.
    `,
  )

  const now = new Date()

  const nextcloudUsernames = new Set(await retrieveUsernames())
  const deputesActifs = await retrieveDeputesActifs()

  console.log("Loading shared calendars…")
  const calendars = await retrieveUserCalendars({
    username: nextcloud.user,
    password: nextcloud.password,
  })
  const calendarByDisplayName = calendars.reduce(
    (calendarByDisplayName, calendar) => {
      calendarByDisplayName[calendar.displayName] = calendar
      return calendarByDisplayName
    },
    {},
  )

  for (let depute of deputesActifs) {
    const infosDepute = infosDeputeByUid[depute.uid]
    if (infosDepute === undefined) {
      continue
    }
    const email = infosDepute.emails.filter(email =>
      email.endsWith("@assemblee-nationale.fr"),
    )[0]
    if (email === undefined) {
      continue
    }
    if (!nextcloudUsernames.has(email)) {
      // Député doesn't have a Nextcloud account yet.
      continue
    }
    const deputeCalendar =
      calendarByDisplayName[
        `${depute.etatCivil.ident.nom}, ${depute.etatCivil.ident.prenom}`
      ]
    if (deputeCalendar !== undefined) {
      await shareCalendar(deputeCalendar, { samlId: email })
    }

    const deputeCalendarsDisplayNames = new Set()
    for (let mandat of depute.mandats || []) {
      if (mandat.dateDebut > now || (mandat.dateFin && mandat.dateFin < now)) {
        continue
      }
      for (let organeRef of mandat.organesRefs) {
        const organe = organeByUid[organeRef]
        if (organe === undefined) {
          continue
        }
        const deputeCalendarDisplayName = organe.libelleAbrege
        const calendar = calendarByDisplayName[deputeCalendarDisplayName]
        if (calendar === undefined) {
          continue
        }
        deputeCalendarsDisplayNames.add(deputeCalendarDisplayName)
      }
    }
    for (let deputeCalendarDisplayName of [
      ...deputeCalendarsDisplayNames,
    ].sort()) {
      await shareCalendar(
        calendarByDisplayName[deputeCalendarDisplayName],
        email,
      )
    }
  }
}

async function retrieveDeputesActifs() {
  const dataDir = options.dataDir
  assert(dataDir, "Missing argument: data directory")
  const now = new Date()
  const deputesActifs = Object.values(acteurByUid)
    .filter(acteur => {
      const { mandats } = acteur
      if (!mandats) {
        return false
      }
      const mandatDeputeActif = mandats.filter(
        mandat =>
          mandat.xsiType === MandatXsiType.MandatParlementaireType &&
          mandat.legislature === options.legislature &&
          mandat.typeOrgane === CodeType.Assemblee &&
          mandat.dateDebut <= now &&
          (!mandat.dateFin || mandat.dateFin >= now),
      )[0]
      if (mandatDeputeActif === undefined) {
        return false
      }
      const mandatGroupeParlementaireActif = mandats.filter(
        mandat =>
          mandat.xsiType === MandatXsiType.MandatSimpleType &&
          mandat.legislature === options.legislature &&
          mandat.typeOrgane === CodeType.Gp &&
          mandat.dateDebut <= now &&
          (!mandat.dateFin || mandat.dateFin >= now) &&
          mandat.organesRefs.some(
            organeRef => organeRef == options.groupeParlementaire,
          ),
      )[0]
      if (mandatGroupeParlementaireActif === undefined) {
        return false
      }
      return true
    })
    .sort((a, b) => a.uid.localeCompare(b.uid))

  const firstUid = options.uid
  let skip = !!firstUid
  for (let depute of deputesActifs) {
    if (skip) {
      if (depute.uid === firstUid) {
        skip = false
      } else {
        continue
      }
    }
  }

  return deputesActifs
}

main().catch(error => {
  console.log(error)
  process.exit(1)
})
