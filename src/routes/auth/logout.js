export function get(req, res) {
  req.logout()
  res.writeHead(302, {
    Location: req.baseUrl + "/",
  })
  return res.end()
}
