import passport from "passport"

export function post(req, res, next) {
  passport.authenticate("saml", { failureRedirect: "/", failureFlash: true })(
    req,
    res,
    function(err, user, info) {
      console.log("auth/login-callback", err, user, info)
      if (err) {
        return next(err)
      }
      if (!user) {
        // res.setHeader("Content-Type", "application/json; charset=utf-8")
        // return res.end(JSON.stringify({ error: info }, null, 2))
        res.writeHead(302, {
          Location: req.baseUrl + "/",
        })
        return res.end()
      }
      req.login(user, function(err) {
        if (err) {
          return next(err)
        }
        // res.setHeader("Content-Type", "application/json; charset=utf-8")
        // return res.end(JSON.stringify(user, null, 2))
        res.writeHead(302, {
          Location: req.baseUrl + "/",
        })
        return res.end()
      })
    },
  )
}
