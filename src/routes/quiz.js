import serverConfig from "../server_config"

const { quiz } = serverConfig

export async function get(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthenticated user",
          },
        },
        null,
        2,
      ),
    )
  }

  let url = null
  if (quiz !== null) {
    // eslint-disable-next-line no-undef
    const response = await fetch(
      new URL(
        `api/applications/${encodeURIComponent(quiz.segment)}/start`,
        quiz.url,
      ).toString(),
      {
        body: JSON.stringify(
          {
            applicationToken: quiz.token,
            username: user.nameId,
          },
          null,
          2,
        ),
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        method: "POST",
      },
    )
    if (!response.ok) {
      res.writeHead(response.status, {
        "Content-Type": "application/json; charset=utf-8",
      })
      return res.end(
        JSON.stringify(
          {
            error: {
              code: response.status,
              message: response.statusText,
              body: await response.text(),
            },
          },
          null,
          2,
        ),
      )
    }
    const { sessionToken } = await response.json()
    url = new URL(
      `embed/quiz?session=${encodeURIComponent(sessionToken)}`,
      quiz.url,
    ).toString()
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify(
      {
        url,
      },
      null,
      2,
    ),
  )
}

// vim: set ts=2 sw=2 sts=2 et:
