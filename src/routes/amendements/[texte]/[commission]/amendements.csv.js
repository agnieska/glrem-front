import { db } from "../../../../database"
import { jsonError } from "../../../../lib/errors"
import { commAbrev } from "../../../../lib/data"
import sort from "../../../../lib/sort"

const csv_keys = ["amdt_id", "amdt_division", "amdt_division_place", "amdt_alinea", "amdt_alinea_place",
  "amdt_titre", "amdt_etat", "amdt_sort", "auteur", "auteur_alpha"]

export async function get(req, res) {

  const { user } = req
  if (!user) {
    return jsonError(res, 401, "Unauthenticated user")
  }

  const auteurId = req.user && req.user.idTribun &&
                   (req.user.role === "DEPUTE" || req.user.role === "COLLABORATEUR_DEPUTE")
                     ? "PA" + req.user.idTribun
                     : null

  // Check params
  const { texte, commission } = req.params

  if (!/^(?:TEMP)?[0-9]+$/.test(texte) || !(commission in commAbrev)) {
    return jsonError(res, 400, "Bad parameters")
  }

  const texte_amendable = await db.oneOrNone(
    ` SELECT *
      FROM textes_amendables
      LEFT JOIN textes ON (txta_txt = txt_id)
      WHERE txt_id = $<txt_id> AND txta_commission = $<txta_commission>
    `,
    {
      txt_id: texte,
      txta_commission: commAbrev[commission],
    },
  )
  if (!texte_amendable) {
    return jsonError(res, 404, "Missing text")
  }

  const paramAmendements = {
    amdt_texte_id: texte_amendable.txta_id,
    amdt_auteur: auteurId,
  }
  const estResponsable = (req.user && req.user.role === "COLLABORATEUR_GROUPE")
                         || (texte_amendable.txt_responsables || []).indexOf(auteurId) !== -1
  if (!estResponsable) {
    return jsonError(res, 403, "Unauthorised")
  }

  const amendements = (await db.manyOrNone(
    ` SELECT amendements.*, auteurs.*
      FROM amendements
      LEFT JOIN auteurs ON (amdt_auteur = aut_id)
      WHERE amdt_texte_id = $<amdt_texte_id> AND amdt_etat IN ('DEPOSE', 'DISCUTE')
    `,
    paramAmendements,
  ))
  .sort(sort.sort_amendements)
  .map(
    amendement => {
      amendement.auteur = amendement.aut_civ + " " + amendement.aut_nom + " " + amendement.aut_prenom
      amendement.auteur_alpha = amendement.aut_alpha
      let values = []
      for (let i of csv_keys) {
        values.push((amendement[i] || "").replace(/"/g, "\"\""))
      }
      return "\"" + values.join("\";\"") + "\""
    }
  ).join("\n")
  const headers = "\"" + csv_keys.map(
      header => header.replace(/^amdt_/, "").replace("titre", "sujet")
    ).join("\";\"") + "\"\n"

  res.writeHead(200, {
    "Content-Type": "text/csv; charset=utf-8; header=present",
    "Content-Disposition": `attachment; filename="amendements-${texte}-${commission}.csv"`,
  })
  res.end(
    headers + amendements
  )
}

// vim: set ts=2 sw=2 sts=2 et:
