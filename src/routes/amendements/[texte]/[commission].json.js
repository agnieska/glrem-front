import { db } from "../../../database"
import { jsonError } from "../../../lib/errors"
import { commAbrev } from "../../../lib/data"
import sort from "../../../lib/sort"

export async function get(req, res) {

  const { user } = req
  if (!user) {
    return jsonError(res, 401, "Unauthenticated user")
  }

  const auteurId = req.user && req.user.idTribun &&
                   (req.user.role === "DEPUTE" || req.user.role === "COLLABORATEUR_DEPUTE")
                     ? "PA" + req.user.idTribun
                     : null

  // Check params
  const { texte, commission } = req.params

  if (!/^(?:TEMP)?[0-9]+$/.test(texte) || !(commission in commAbrev)) {
    return jsonError(res, 400, "Bad parameters")
  }

  const texte_amendable = await db.oneOrNone(
    ` SELECT *
      FROM textes_amendables
      LEFT JOIN textes ON (txta_txt = txt_id)
      WHERE txt_id = $<txt_id> AND txta_commission = $<txta_commission>
    `,
    {
      txt_id: texte,
      txta_commission: commAbrev[commission],
    },
  )
  if (!texte_amendable) {
    return jsonError(res, 404, "Missing text")
  }

  const estResponsable = (req.user && req.user.role === "COLLABORATEUR_GROUPE")
                         || (texte_amendable.txt_responsables || []).indexOf(auteurId) !== -1

  const amendements = (await db.manyOrNone(
    ` SELECT amendements.*, auteurs.*
      FROM amendements
      LEFT JOIN auteurs ON (amdt_auteur = aut_id)
      WHERE amdt_texte_id = $<amdt_texte_id>
        AND (amdt_etat IN ('DEPOSE', 'DISCUTE') OR amdt_auteur = $<auteurId>)
    `,
    {
      amdt_texte_id: texte_amendable.txta_id,
      auteurId,
      estResponsable,
    }
  ))
  .map(
    amendement => {
      if (!estResponsable && amendement.amdt_auteur !== auteurId && texte_amendable.txta_amendable) {
        delete amendement.amdt_dispositif
      }
      return amendement
    }
  )
  .sort(sort.sort_amendements)

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(
    JSON.stringify({
      status: "success",
      texteMetadata: texte_amendable,
      amendements,
    }),
  )
}

// vim: set ts=2 sw=2 sts=2 et:
