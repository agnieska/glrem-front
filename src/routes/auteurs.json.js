import { db } from "../database"

export async function get(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthenticated user",
          },
        },
        null,
        2,
      ),
    )
  }

  res.writeHead(200, {
    "Cache-Control": "public, max-age=300, s-maxage=120",
    "Content-Type": "application/json; charset=utf-8",
  })
  const auteursDB = await db.manyOrNone(
    `
      SELECT aut_id, aut_civ, aut_nom, aut_prenom, aut_alpha
      FROM auteurs
      ORDER BY aut_alpha
    `,
  )
  let auteurs = auteursDB.map(s => {
    s.aut_id = s.aut_id.trim()
    return s
  })
  res.end(JSON.stringify(auteurs))
}

// vim: set ts=2 sw=2 sts=2 et:
