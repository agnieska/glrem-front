import fetch from "node-fetch"
import url from "url"

import serverConfig from "../../server_config"

const { zendesk } = serverConfig

export async function get(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthenticated user",
          },
        },
        null,
        2,
      ),
    )
  }

  const articles = []
  if (zendesk !== null) {
    const response = await fetch(
      url.resolve(
        zendesk.url,
        `api/v2/help_center/fr/categories/${
          zendesk.actualitesId
        }/articles.json?sort_by=created_at&sort_order=desc`,
      ),
      {
        headers: {
          Authorization:
            "Basic " +
            Buffer.from(zendesk.email + "/token:" + zendesk.apiToken).toString(
              "base64",
            ),
        },
        method: "GET",
      },
    )
    if (!response.ok) {
      console.error(response.status, response.statusText)
      console.error(await response.text())
    } else {
      const result = await response.json()
      articles.push(...result.articles)
    }
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(articles, null, 2))
}
