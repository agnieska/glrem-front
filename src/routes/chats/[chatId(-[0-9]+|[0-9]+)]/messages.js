import { telegramClient, setupMessageContent } from "../../../lib/telegram"
// import {
//   validateStringToNumber,
//   validateChain,
//   validateInteger,
//   validateMaybeTrimmedString,
//   validateString,
//   validateTest,
// } from "../../../validators/core"

export async function get(req, res) {
  const { user } = req
  if (!user) {
    res.writeHead(401, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          error: {
            code: 401,
            message: "Unauthenticated user",
          },
        },
        null,
        2,
      ),
    )
  }

  const { chatId } = req.params

  const [query, error] = validateQuery(req.query)
  if (error !== null) {
    console.error(
      `Error in ${req.path} query:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(error, null, 2)}`,
    )
    res.writeHead(400, {
      "Content-Type": "application/json; charset=utf-8",
    })
    return res.end(
      JSON.stringify(
        {
          ...query,
          error: {
            code: 400,
            details: error,
            message: "Invalid query",
            path: req.path,
          },
        },
        null,
        2,
      ),
    )
  }

  const limit = 15

  let history = await telegramClient.fetch({
    "@type": "getChatHistory",
    chat_id: chatId,
    from_message_id: 0, // chat.last_message.id,
    offset: 0,
    limit,
    only_local: true,
  })
  const messages = history.messages

  if (messages.length === 1) {
    history = await telegramClient.fetch({
      "@type": "getChatHistory",
      chat_id: chatId,
      from_message_id: messages[messages.length - 1].id,
      offset: 0,
      limit: limit - messages.length,
      only_local: true,
    })
    messages.push(...history.messages)
  }

  for (let message of messages) {
    await setupMessageContent(message.content)
  }

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  res.end(JSON.stringify(messages, null, 2))
}

function validateQuery(query) {
  if (query === null || query === undefined) {
    return [query, "Missing query"]
  }
  if (typeof query !== "object") {
    return [query, `Expected an object, got ${typeof query}`]
  }

  query = {
    ...query,
  }
  const remainingKeys = new Set(Object.keys(query))
  const errors = {}

  // {
  //   const key = "id"
  //   remainingKeys.delete(key)
  //   const [value, error] = validateChain([
  //     validateString,
  //     validateStringToNumber,
  //     validateInteger,
  //     validateTest(value => value >= 0, "Le nombre doit être positif ou nul."),
  //   ])(query[key])
  //   query[key] = value
  //   if (error !== null) {
  //     errors[key] = error
  //   }
  // }

  // {
  //   const key = "q"
  //   remainingKeys.delete(key)
  //   const [value, error] = validateMaybeTrimmedString(query[key])
  //   query[key] = value
  //   if (error !== null) {
  //     errors[key] = error
  //   }
  // }

  // {
  //   const key = "year"
  //   remainingKeys.delete(key)
  //   const [value, error] = validateChain([
  //     validateString,
  //     validateStringToNumber,
  //     validateInteger,
  //     validateTest(value => value >= 1700 && value < 2000, "Expected a year between 1700 and 1999"),
  //   ])(query[key])
  //   query[key] = value
  //   if (error !== null) {
  //     errors[key] = error
  //   }
  // }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [query, Object.keys(errors).length === 0 ? null : errors]
}
