import { db } from "../../database"
import { jsonError } from "../../lib/errors"
import { commAbrev } from "../../lib/data"

export async function post(req, res) {

  const { user } = req
  if (!user) {
    return jsonError(res, 401, "Unauthenticated user")
  }

  const auteurId = req.user && req.user.idTribun &&
                   (req.user.role === "DEPUTE" || req.user.role === "COLLABORATEUR_DEPUTE")
                     ? "PA" + req.user.idTribun
                     : null

  // Check params
  const { amendementId, texteId, commission, sort } = req.body

  if (
    !/^(?:TEMP)?[0-9]+$/.test(texteId) ||
    !(commission in commAbrev) ||
    !Number(amendementId) ||
    ["", "Retenu", "Appel", "Non-retenu", "À expertiser"].indexOf(sort) === -1
  ) {
    return jsonError(res, 400, "Bad parameters")
  }

  const responsables_texte = await db.oneOrNone(
    ` SELECT txt_responsables
      FROM textes
      INNER JOIN textes_amendables ON (txta_txt = txt_id)
      INNER JOIN amendements ON (amdt_texte_id = txta_id)
      WHERE txta_txt = $1 AND txta_commission = $2 AND amdt_id = $3
    `,
    [texteId, commAbrev[commission], amendementId],
  )
  if (!responsables_texte) {
    return jsonError(res, 400, "Bad parameters")
  }
  const estResponsable = (req.user && req.user.role === "COLLABORATEUR_GROUPE")
                         || (responsables_texte.txt_responsables || []).indexOf(auteurId) !== -1
  if (!estResponsable) {
    return jsonError(res, 403, "Unauthorised user")
  }

  await db.oneOrNone(
    ` UPDATE amendements
        SET amdt_sort = $1,
            amdt_date_sort = 'NOW()'
      WHERE amdt_id = $2
    `,
    [sort, amendementId],
  )

  res.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
  })
  return res.end(JSON.stringify({}))
}

// vim: set ts=2 sw=2 sts=2 et:
