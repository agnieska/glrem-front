# sapper-template

## Initialise the database

1. Install PostgreSQL 9.6 or greater, connect as root, and create the database:
  ```bash
   $ sudo su - postgres
   # createuser lrem -P # and enter the password
   # createdb -O lrem lrem
   # exit
  ```
2. Initialize the database:
   ```bash
   $ psql -h 127.0.0.1 -U lrem -W lrem # and enter the password
   > \i /path/to/app/then/src/lib/schema.sql
   > INSERT INTO version VALUES (0);
   > \q
   ```

## Launch the Sapper app

Clone the repository, then:

```bash
cd /path/to/app/
npm install # or yarn!
```

Copy file `example.env` to `.env` and edit `.env` file, then:

```bash
npm run dev
```

Open up [localhost:3000](http://localhost:3000) and start clicking around.

Consult [sapper.svelte.technology](https://sapper.svelte.technology) for help getting started.

## Quick demo links

[Create an amendment on the text 1926](http://localhost:3000/amendement/1926/SEANCE/nouveau)

[List the amendments on the text 1926](http://localhost:3000/amendements/1926/SEANCE)

## Retrieve "textes lois" and extract informations

### Execute retrieval scripts

```bash
# Retrieve open data (in JSON format) from Assemblée nationale's website:
npx babel-node -- node_modules/@tricoteuses/assemblee/lib/scripts/retrieve_open_data.js --fetch ../assemblee-data/
# Split open data big files into small files (one for each entity).
npx babel-node -- node_modules/@tricoteuses/assemblee/lib/scripts/reorganize_data.js ../assemblee-data/
# Validate & clean JSON data:
npx babel-node -- node_modules/@tricoteuses/assemblee/lib/scripts/clean_reorganized_data.js ../assemblee-data/
# Retrieve députés' pictures from Assemblée nationale's website:
npx babel-node -- node_modules/@tricoteuses/assemblee/lib/scripts/retrieve_deputes_photos.js --fetch ../assemblee-data/
# Retrieve députés' non open data informations (email...) from Assemblée nationale's website:
npx babel-node -- node_modules/@tricoteuses/assemblee/lib/scripts/retrieve_deputes_infos.js --fetch --parse ../assemblee-data/
# Retrieve députés' informations to populate the table `auteurs` in database:
npx babel-node src/scripts/update_db_auteurs.js ../assemblee-data/
# Create JSON file containing active "députés" and "organes":
npx babel-node src/scripts/write_deputes_et_organes_json.js ../assemblee-data/
# Retrieve content of "textes de lois" from Assemblée nationale's website:
npx babel-node src/scripts/retrieve_textes_lois.js --fetch --parse ../assemblee-data/
```

#### Synchronize calendars using Assemblée nationale data

Upsert the events of all the Nextcloud calendars:
```bash
npx babel-node src/scripts/update_nextcloud_calendars.js ../assemblee-data/
```

### Simulate being authenticated as a user (DEV)

Add in `.env` file:
```bash
INTRANETLREM_DEV_USER='{"email": "X@Y.fr", "fullName": "Prénom Nom", "manager": "Z", "memberOf": [], "nameId": "T", "samlId": "T@assemblee-nationale.fr"}'
```

And touch a file (e.g. `src/server.js`) to relaunch Sapper compilation.

To temporarily change the user (as dev), connect to the database and execute one of the following SQL command:
```sql
UPDATE users SET user_role = 'COLLABORATEUR_GROUPE' WHERE user_name_id = 'XXX';
UPDATE users SET user_role = 'COLLABORATEUR_DEPUTE', user_id_tribun = '000000' WHERE user_name_id = 'XXX';
```

The Tribun ID can be found on Wikidata within the property "[identifiant Assemblée nationale (France)](https://www.wikidata.org/wiki/Property:P4123)", or in the URLs of the Assemblée’s website on each député page (http://www2.assemblee-nationale.fr/deputes/fiche/OMC\_PA000000).

## Structure

Sapper expects to find two directories in the root of your project —  `src` and `static`.


### src

The [src](src) directory contains the entry points for your app — `client.js`, `server.js` and (optionally) a `service-worker.js` — along with a `template.html` file and a `routes` directory.


#### src/routes

This is the heart of your Sapper app. There are two kinds of routes — *pages*, and *server routes*.

**Pages** are Svelte components written in `.html` files. When a user first visits the application, they will be served a server-rendered version of the route in question, plus some JavaScript that 'hydrates' the page and initialises a client-side router. From that point forward, navigating to other pages is handled entirely on the client for a fast, app-like feel. (Sapper will preload and cache the code for these subsequent pages, so that navigation is instantaneous.)

**Server routes** are modules written in `.js` files, that export functions corresponding to HTTP methods. Each function receives Express `request` and `response` objects as arguments, plus a `next` function. This is useful for creating a JSON API, for example.

There are three simple rules for naming the files that define your routes:

* A file called `src/routes/about.html` corresponds to the `/about` route. A file called `src/routes/blog/[slug].html` corresponds to the `/blog/:slug` route, in which case `params.slug` is available to the route
* The file `src/routes/index.html` (or `src/routes/index.js`) corresponds to the root of your app. `src/routes/about/index.html` is treated the same as `src/routes/about.html`.
* Files and directories with a leading underscore do *not* create routes. This allows you to colocate helper modules and components with the routes that depend on them — for example you could have a file called `src/routes/_helpers/datetime.js` and it would *not* create a `/_helpers/datetime` route


### static

The [static](static) directory contains any static assets that should be available. These are served using [sirv](https://github.com/lukeed/sirv).

In your [service-worker.js](app/service-worker.js) file, you can import these as `files` from the generated manifest...

```js
import { files } from '../__sapper__/service-worker.js';
```

...so that you can cache them (though you can choose not to, for example if you don't want to cache very large files).


## Bundler config

Sapper uses Rollup or webpack to provide code-splitting and dynamic imports, as well as compiling your Svelte components. With webpack, it also provides hot module reloading. As long as you don't do anything daft, you can edit the configuration files to add whatever plugins you'd like.


## Production mode and deployment

To start a production version of your app, run `npm run build && npm start`. This will disable live reloading, and activate the appropriate bundler plugins.

You can deploy your application to any environment that supports Node 8 or above. As an example, to deploy to [Now](https://zeit.co/now), run these commands:

```bash
npm install -g now
now
```


## Using external components with webpack

When using Svelte components installed from npm, such as [@sveltejs/svelte-virtual-list](https://github.com/sveltejs/svelte-virtual-list), Svelte needs the original component source (rather than any precompiled JavaScript that ships with the component). This allows the component to be rendered server-side, and also keeps your client-side app smaller.

Because of that, it's essential that webpack doesn't treat the package as an *external dependency*. You can either modify the `externals` option in [webpack/server.config.js](webpack/server.config.js), or simply install the package to `devDependencies` rather than `dependencies`, which will cause it to get bundled (and therefore compiled) with your app:

```bash
yarn add -D @sveltejs/svelte-virtual-list
```


## Bugs and feedback

Sapper is in early development, and may have the odd rough edge here and there. Please be vocal over on the [Sapper issue tracker](https://github.com/sveltejs/sapper/issues).
